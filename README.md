# HW 2 and 3 with dataset HOME CREDIT from kaggle

Project Structure
README.md - The top-level README for developers using this project.

data

download_data.sh - Instruction for downloading data from kaggle

notebooks - Jupyter notebooks. Keep my homework process.

requirements.txt -  The requirements file for reproducing the analysis environment, e.g. generated with `pip freeze > requirements.txt`
 
src -  Source code for use in this project. Just now is empty 

__init__.py    <- Makes src a Python module

